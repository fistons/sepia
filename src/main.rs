use std::fs::File;
use std::fs::OpenOptions;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Read;
use std::io::Write;

struct Image {
    data: Vec<u8>,
    width: usize,
    height: usize,
}

fn xy_to_index(x: usize, y: usize, width: usize) -> usize {
    (y * 3 * width) + (3 * x)
}

fn load_image(file_path: &str) -> std::io::Result<Image> {
    let mut reader = BufReader::new(File::open(file_path)?);
    let mut buffer = String::new();
    reader.read_line(&mut buffer)?;
    if &buffer[..buffer.len() - 1] != "P6" {
        eprintln!("Input File is not a ppm file bro");
        std::process::exit(-1);
    }

    buffer.clear();

    reader.read_line(&mut buffer)?;
    let (width, height) = buffer[..buffer.len() - 1]
        .split_once(' ')
        .expect("Input File is not a ppm file bro");
    let (width, height): (usize, usize) = (
        width.parse().expect("Not a valid ppm width"),
        height.parse().expect("Not a valid ppm height"),
    );
    buffer.clear();

    reader.read_line(&mut buffer)?;
    buffer.clear();

    let mut data = vec![];
    reader.read_to_end(&mut data)?;

    Ok(Image {
        data,
        width,
        height,
    })
}

fn save_image(file_path: &str, image: &[u8], width: usize, height: usize) -> std::io::Result<()> {
    let mut file = OpenOptions::new()
        .write(true)
        .create(true)
        .open(file_path)?;

    file.write_all(format!("P6\n{} {}\n255\n", width, height).as_bytes())?;
    file.write_all(image)?;
    Ok(())
}

fn calcul(image: &[u8], width: usize, x: usize, y: usize, new_image: &mut [u8]) {
    let index = xy_to_index(x, y, width);

    let red = image[index] as f32;
    let green = image[index + 1] as f32;
    let blue = image[index + 2] as f32;

    let mut new_red = 0.0;
    let mut new_green = 0.0;
    let mut new_blue = 0.0;

    for i in 0..=1000 {
        // Useless loop to simultate heavy calculation
        let fact = 393.0 / i as f32;
        new_red = fact * red + 0.769 * green + 0.189 * blue;

        let fact = 686.0 / i as f32;
        new_green = 0.349 * red + fact * green + 0.168 * blue;

        let fact = 131.0 / i as f32;
        new_blue = 0.272 * red + 0.534 * green + fact * blue;
    }

    new_image[index] = new_red as u8;
    new_image[index + 1] = new_green as u8;
    new_image[index + 2] = new_blue as u8;
}

/*----- Zone to modify -----*/

/*----- End of zone to modify -----*/

fn main() -> std::io::Result<()> {
    let image = load_image("image.ppm")?;
    /*----- Zone to modify -----*/

    let mut new_image = vec![0; image.data.len()];
    for y in 0..image.height {
        for x in 0..image.width {
            calcul(&image.data, image.width, x, y, &mut new_image);
        }
    }
    /*----- End of zone to modify -----*/

    save_image("sepia_rust.ppm", &new_image, image.width, image.height)?;
    Ok(())
}
